import Head from 'next/head'
import Header from './header'

const ViewerLayout = (props) => (
  <>
    <Head>
      <title>CHARP</title>
    </Head>

    <Header />

    <main>
      <div id="viewerID" className="viewerContainer">{props.children}</div>
    </main>

    <style>{`
      header {
        width: 100vw;
        height: 50px;
      }
      
      .viewerContainer {
        background-color: var(--primary-background-color);
        width: 100vw;
        height: calc(100vh - 50px);
      }
    `}</style>
  </>
)

export default ViewerLayout
